
package filecompression;

/**
 *
 * @author Zivlak
 */
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Base64;
import java.util.PriorityQueue; 
import java.util.Comparator; 
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

//Left i right HuffmanNode koristimo za kreiranje stabla
class HuffmanNode { 

	int data; 
	char c; 

	HuffmanNode left; 
	HuffmanNode right; 
} 

//Komparator koristimo radi poredjena cvorova Huffman-ovog stabla
class MyComparator implements Comparator<HuffmanNode> { 
        @Override
	public int compare(HuffmanNode x, HuffmanNode y) 
	{ 

		return x.data - y.data; 
	} 
} 

public class FileCompression implements Comparator<String>{ 

        static byte[] arr;
        static int addBytes = 0;
        static File file = new File("C:\\Users\\Zivlak\\Documents\\NetBeansProjects\\FileCompression\\src\\filecompression\\proba.txt");
        //Rekurzivna funkcija za ispis huffman-ovog koda
        //prolazenjem kroz stablo.
	public static void printCode(HuffmanNode root, String s) 
	{ 

		 
            
                //U baznom slucaju, ako su lijevi i desni cvor null
                //to znaci da su listovi i printamo kod od s dobijen prolazenjem kroz stablo
		if (root.left == null && root.right == null /*&& Character.isLetter(root.c)*/) { 

			//c je karakter u cvoru 
			System.out.println(root.c + ":" + s); 

			return; 
		} 

		// ako idemo lijevo, dodajemo "0" kodu. 
		// ako idemo desno, dodajemo "1" kodu. 

		// rekurzivni poziv za lijevo i desno podstablo
		printCode(root.left, s + "0");
		printCode(root.right, s + "1");
                
	}
        
        //Funkcija koja ce nam vratiti mapu karaktera i njihove vjerovatnoce
        public static Map<Character, String> probability(String x){
		
            int [] arr = new int[x.length()];
            double sumFrequencies = 0;
            DecimalFormat df = new DecimalFormat("#.#####"); //Koristimo za ispisivanje 5 decimala
            
            Map<Character, Integer> totalFrequencies = new HashMap<>();
            Map<Character, String> probabilities = new HashMap<>();
            
            //Prolazimo kroz proslijedjeni String x i ASCII kod njegovih karaktera
            //zapisujemo u novi niz.
            for(int i = 0; i < x.length(); i++) {
                    arr[i] = x.charAt(i);
            }
            
            for(int current : arr) {
                //Prolazimo kroz niz ASCII vrijednosti i zapisujemo te karaktere u mapu.
                //Osim toga, koristimo funkciju repetitions koja izracunava broj ponavljanja za svaki karakter.
                totalFrequencies.put((char)current, repetitions(arr, current));

                
            }
            
            for (Map.Entry<Character, Integer> entry : totalFrequencies.entrySet()) {
                    //Racunanje sume frekvencija
                    int frek = entry.getValue();
                    sumFrequencies  += frek;
                    
            }
            for(int current : arr) {
           
                double frek = repetitions(arr, current);
                double prob = frek/sumFrequencies ;
                //U vjerovatnoce stavljamo taj isti karakter i njegovu vjerovatnocu, 
                //koju dobijemo kada njegov broj ponavljanja podijelimo sa ukupnim brojem ponavljanja.
                probabilities.put((char)current, df.format(prob));
                

            }
            System.out.println("Suma frekvencija je: " + sumFrequencies );
            System.out.println("Frekvencije karaktera: " +totalFrequencies.toString());
            
            //Sorted maps
            Map<Character, String> sortedMap = probabilities.entrySet().stream()
                         .sorted(Entry.comparingByValue())
                         .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

            return sortedMap;
            //Vracamo sortiranu mapu iz razloga sto ce 0 i 1 onda biti pravilno dodijeljene.
            //One se dodjeljuju po redu, ukoliko nije sortirana, bitovi nece biti pravilno rasporedjeni.
        }
        
        //Funkcija za racunanje frekvencija(racuna preko ASCII vrijednosti karaktera)
        public static int repetitions(int[]arr, int value) {
            
            int counter = 0;
            for(int i = 0; i < arr.length; i++) {
                    if( arr[i] == value ) {
                        counter++;
                    }
            }

            return counter;
	}
	// main function 
	public static void main(String[] args) throws FileNotFoundException, IOException 
	{       
                String stringFile = "";
                File picture = new File("C:\\Users\\Zivlak\\Documents\\NetBeansProjects\\FileCompression\\src\\filecompression\\sand.png");
                //Citanje fajla u byte arr
                byte [] readBytes = readByte(picture);
               
                
                //ENCODE  -----------------------------------------------
                byte [] encoded = encode(readBytes);
                for(int i = 0; i < encoded.length; i++){
               
                    stringFile += (char)encoded[i];
                }
                System.out.println("Encoded string:");
                System.out.println(stringFile);   
                
                System.out.println("");
                
                //Koristimo encoded string da kreiramo Huffman-ov rijecnik
                Map<Character, String> dictionary = probability(stringFile);
                
		
                int numberOfCharacters = dictionary.size(); //Broj karaktera u rijecniku
                System.out.println("Number of characters: " + numberOfCharacters);
                
              
		PriorityQueue<HuffmanNode> q = new PriorityQueue<HuffmanNode>(numberOfCharacters, new MyComparator()); 
                
                for (Map.Entry<Character, String> entry : dictionary.entrySet()) {
                    HuffmanNode hn = new HuffmanNode(); 
                    
                    char c = entry.getKey();
                    String st = entry.getValue();
                    double d = Double.parseDouble(st);
                    int i = (int) d;
                    
                    hn.c = c;
                    hn.data = i;
                    
                    hn.left = null;
                    hn.right = null;
                    
                    q.add(hn);
                }       


		HuffmanNode root = null;
                
		while (q.size() > 1) { 

                    HuffmanNode x = q.peek(); 
                    q.poll(); 

                    HuffmanNode y = q.peek(); 
                    q.poll(); 

                    HuffmanNode f = new HuffmanNode(); 

                    
                    f.data = x.data + y.data; 
                    f.c = '-'; 

                    f.left = x; 

                    f.right = y; 

                    root = f; 

                    q.add(f); 
		} 

		 
		printCode(root, ""); 
                //Konvertovanje Huffman-ovog rijecnika u byte array i zapisivanje u kompresovani fajl.
                byte [] compressed = convert(dictionary);
                try (FileOutputStream fos = new FileOutputStream("C:\\Users\\Zivlak\\Documents\\NetBeansProjects\\FileCompression\\src\\filecompression\\sandCompressed.png")) {
                    fos.write(compressed);
                        //fos.close(); 
                }
                //Dekodovanje Huffman-ovog konvertovanog rijecnika i zapisivanje u dekompresovani fajl.
                byte[] decoded = decode(encoded);
                try (FileOutputStream fos = new FileOutputStream("C:\\Users\\Zivlak\\Documents\\NetBeansProjects\\FileCompression\\src\\filecompression\\sandDecompressed.png")) {
                    fos.write(decoded);
                    //fos.close(); 
                }
                  
 
                System.out.println("rest of the code");  
                      
        }
        
    //Komparator
    @Override
    public int compare(String t, String t1) {
        int first = Integer.parseInt(t);
        int second = Integer.parseInt(t1);
        
        return t.compareTo(t1);
    }
    
    //Zapisivanje byte arr u fajl
    public static void writeByte(byte[] bytes) 
    { 
            try (OutputStream os = new FileOutputStream(file)) {
                os.write(bytes);
                System.out.println("Successfully" + " byte inserted");
            } catch (IOException e) { 
            System.out.println("Exception: " + e); 
        } 
         
    }
    
    //Citanje bajtova iz fajla
    public static byte[] readByte(File file) throws FileNotFoundException, IOException{
        
        byte[] bytesArray = new byte[(int) file.length()]; 

            try (FileInputStream fis = new FileInputStream(file)) {
                fis.read(bytesArray); 
            } 

        return bytesArray;
    }
    //Enkodovanje
    public static byte[] encode(byte[]readBytes){
        byte[] encoded = Base64.getEncoder().encode(readBytes);
        
        return encoded;
    }
    //Dekodovanje
    public static byte[] decode(byte[] encoded){
        byte[] decoded = Base64.getDecoder().decode(encoded);
            
        return decoded;
    }
    
    //Konvertovanje HashMap-e u byte array
    public static byte[] convert(Object obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
                oos.writeObject(obj);
                oos.flush();
            }
        return baos.toByteArray();
    }

    public static Object convert(byte[] bytes) throws Exception {
        Object object;
            try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
                object = ois.readObject();
            }
        return object;
    }
}
    

    


        
        

